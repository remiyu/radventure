require 'securerandom'
require_relative 'room'
require_relative 'roll'

# World consisting of sets of rooms
class World
  class << self
    def seed(size)
      new(size: size)
    end

    def randomize
      new
    end
    private :new
  end

  attr_reader :id, :map, :size

  def initialize(size: Roll.range(2, 5))
    @id = SecureRandom.uuid
    @size = size
    @map = build
  end

  def build
    map = []
    Range.new(1, @size).each do
      row = []
      Range.new(1, @size).each do
        row.push Room.random
      end
      map.push row
    end
    map
  end

  def rooms
    tiles = []
    @map.each do |row|
      row.each do |room|
        tiles.push room
      end
    end
    tiles
  end

  def to_s
    map = ''
    @map.each do |row|
      map << '|'
      row.each do |room|
        map << " #{room.room_type}"
      end
      map << ' |\n'
    end
    map
  end
end
