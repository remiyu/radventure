# Roll true or false
module Maybe
  def self.random
    [true, false].sample
  end
end

# Roll numbers
module Roll
  def self.range(x, y)
    Range.new(x, y).to_a.sample
  end

  def self.d20
    range(1, 20)
  end

  def self.d10
    range(1, 10)
  end

  def self.d8
    range(1, 8)
  end

  def self.d6
    range(1, 6)
  end

  def self.d4
    range(1, 4)
  end

  def self.initiative
    d20
  end

  def self.crit?
    d20 == 20
  end
end
