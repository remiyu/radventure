# Player class
class Player
  attr_reader :stats, :bag, :gear
  attr_accessor :name, :profession, :base_hp, :base_mana

  def initialize(name)
    @name = name
    @profession = nil
    @base_hp = 100
    @base_mana = 50
    @bag = []
    @stats = { chc: 50,
               chd: 150,
               level: 1 }
    %i[vit int str agi ap xp].each { |stat| @stats[stat] = 0 }
    @gear = {}
    %i[helm chest neck ring boots weapon].each { |slot| @gear[slot] = :empty }
  end

  def interact(room)
    item = room.search
    bag.push item if item
  end
end
