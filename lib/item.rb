
# Item superclass
class Item
  attr_reader :type, :rarity

  TYPES = %i[weapon armor].freeze
  RARITY = %i[common magic rare exotic].freeze

  def initialize
    @type     = TYPES.sample
    @rarity   = RARITY.sample
    @equipped = false
  end

  def equipped?
    @equipped
  end

  def equip
    @equipped = true unless equipped?
  end

  def unequip
    @equipped = false unless equipped? == false
  end

  def info
    { type: @type,
      rarity: @rarity,
      equpped: @equipped }
  end
end
