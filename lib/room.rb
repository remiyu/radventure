require 'securerandom'
require_relative 'roll'
require_relative 'item'

# Room tile class
class Room
  class << self
    def lootable
      new(lootable: true)
    end

    def occupied
      new(occupied: true)
    end

    def random
      new
    end

    private :new
  end

  attr_reader :id, :room_type

  def initialize(lootable: Maybe.random, occupied: Maybe.random)
    types = {  empty: 0,
               hall: 1,
               study: 2,
               library: 3,
               bedroom: 4 }

    @id = SecureRandom.uuid
    @room_type = types.keys.sample.to_s
    @lootable = lootable
    @occupied = occupied
    @searched = false
    @visited = false
  end

  def search
    return nil if searched?
    @searched = true
    Item.new if lootable?
  end

  def searched?
    @searched
  end

  def visited?
    @visited
  end

  def lootable?
    @lootable
  end

  def occupied?
    @occupied
  end
end
