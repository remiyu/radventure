$LOAD_PATH.push File.expand_path(File.dirname(__FILE__) + '/../lib')
require 'minitest/autorun'
require 'units'
require 'world'
require 'ability'
require 'room'
require 'item'
require 'roll'

describe Player do
  player = Player.new 'John Doe'

  it 'has a name' do
    assert player.name.is_a? String
    assert_equal player.name, 'John Doe'
  end

  it 'has health and mana' do
    assert player.base_hp.is_a? Integer
    assert player.base_mana.is_a? Integer
  end

  %i[vit int str agi ap xp chc chd level].each do |stat|
    it "has a stat for #{stat}" do
      assert player.stats[stat].is_a? Integer
    end
  end

  it 'has a bag' do
    assert player.bag.is_a? Array
  end

  it 'can acquire an item from a room' do
    player.interact Room.lootable
    assert_equal player.bag.length, 1
  end

  it 'has a hash of equipment' do
    assert player.gear.is_a? Hash
  end

  it 'has all empty equipment slots' do
    player.gear.keys { |slot| assert_equal player.gear[slot], :empty }
  end
end

describe World do
  world = World.randomize

  it 'has a UUID' do
    assert world.id.is_a? String
  end

  it 'has a size' do
    assert world.size.is_a? Integer
  end

  it 'can be created with a specific size' do
    # 100 rooms
    big_world = World.seed 10
    assert_equal big_world.size, 10
  end

  it 'has a grid of rooms' do
    world.rooms.each { |room| assert room.is_a? Room }
  end

  it 'has a way to displaly the world' do
    assert world.to_s.is_a? String
  end
end

describe Room do
  room = Room.random

  it 'has a UUID' do
    assert room.id.is_a? String
  end

  it 'has a room type' do
    assert room.room_type.is_a? String
  end

  it 'has a chance at loot' do
    if room.lootable?
      assert room.lootable?.is_a? TrueClass
    else
      room.lootable?.is_a? FalseClass
    end
  end

  it 'has a chance of being occupied' do
    if room.occupied?
      assert room.occupied?.is_a? TrueClass
    else
      room.occupied?.is_a? FalseClass
    end
  end

  it 'starts out as unsearched' do
    assert_equal room.searched?, false
  end

  it 'starts out as unvisited' do
    assert_equal room.visited?, false
  end

  it 'can be generated with loot' do
    loot_room = Room.lootable
    assert loot_room.lootable?
  end

  it 'can be generated with an occupant' do
    occ_room = Room.occupied
    assert occ_room.occupied?
  end

  it 'gives loot when lootable and status changes to searched' do
    loot_room = Room.lootable
    assert loot_room.search.is_a? Item
    assert loot_room.searched?
    assert_nil loot_room.search
  end
end

describe Ability do
  ability = Ability.new

  it 'has a cost' do
    assert ability.cost.is_a? Integer
  end
end

describe Item do
  item = Item.new

  it 'has a type' do
    assert item.type.is_a? Symbol
  end

  it 'keeps track of being equipped' do
    assert_equal item.equipped?, false
    item.equip
    assert item.equipped?
    item.unequip
    assert_equal item.equipped?, false
  end

  it 'has a rarity' do
    assert item.rarity.is_a? Symbol
  end

  it 'reveals information about itself' do
    assert item.info.is_a? Hash
  end
end

describe Roll do
  it 'can pick from a range of numbers' do
    assert (1..10).to_a.include? Roll.range(1, 10)
  end

  it 'can roll initiative' do
    assert (1..20).to_a.include? Roll.initiative
  end

  it 'has a chance to roll a crit' do
    assert [true, false].include? Roll.crit?
  end

  it 'can roll a crit' do
    crit = false
    crit = Roll.crit? until crit
    assert crit
  end

  it 'can roll a d4' do
    assert (1..4).to_a.include? Roll.d4
  end

  it 'can roll a d8' do
    assert (1..8).to_a.include? Roll.d8
  end

  it 'can roll a d6' do
    assert (1..6).to_a.include? Roll.d6
  end

  it 'can roll a d10' do
    assert (1..10).to_a.include? Roll.d10
  end

  it 'can roll a d20' do
    assert (1..20).to_a.include? Roll.d20
  end
end
